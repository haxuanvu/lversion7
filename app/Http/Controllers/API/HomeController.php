<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;
class HomeController extends Controller
{
    public function get_statistical(Request $request)
    {
        $tomorrow = date("Y-m-d", strtotime(" + 1 day"));

        $data = DB::table('vh_link')->where('hien_thi', 1)->where(function($query){
            $query->orWhere('status', '!=', 1)->orWhereNull('status')->orWhere('live', 1)->orWhere('Linkauto', '!=', '');
        })->get();
        
        $dang_da = DB::table('vh_link')->where('hien_thi', 1)->where(function($query){
                $query->orWhere('live', 1)->orWhere('Linkauto', '!=', '');
            })->count();
        $output = array(
            'dang_da' => $dang_da,
            'sap_dien_ra' => DB::table('vh_link')->where('hien_thi', 1)->where(function($query){
                $query->orWhere('status', '!=', 1)->orWhereNull('status');
            })->where('live', 0)->whereNull('Linkauto')->count(),
            'hot' => $data->where('hot', 1)->count(),
            'blv' => DB::table('vh_link')->where('hien_thi', 1)->where(function($query){
                $query->orWhere('live', 1)->orWhere('Linkauto', '!=', '');
            })->count(),
            'hom_nay' => DB::table('vh_link')->where('hien_thi', 1)->where( DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1)"), date('d/m') )->count(),
            'ngay_mai' => DB::table('vh_link')->where('hien_thi', 1)->where( DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1)"), date('d/m', strtotime($tomorrow)) )->count(),
            'tat_ca' => $data->count(),
        );

        return response()->json([
            'success' => true,
            'data'    => $output,
        ]);
    }
    
    public function get_statistical_link(Request $request, $name)
    {
        $tomorrow = date("Y-m-d", strtotime(" + 1 day"));
        $date_filter = $request->date;

        $output = DB::table('vh_link')->where('hien_thi', 1)
        ->when($name == 'dang_da', function($query){
            $query->where(function($query2){
                $query2->orWhere('live', 1)->orWhere('Linkauto', '!=', '');
            });
        })
        ->when($name == 'sap_dien_ra', function($query){
            $query->where('live', 0)->whereNull('Linkauto')->where(function($query2){
                $query2->orWhere('status', '!=', 1)->orWhereNull('status');
            });
        })
        ->when($name == 'hot', function($query){
            $query->where('hot', 1);
        })
        ->when($name == 'blv', function($query){
            $query->where(function($query2){
                $query2->orWhere('live', 1)->orWhere('Linkauto', '!=', '');
            });
        })
        ->when($name == 'hom_nay', function($query){
            $query->where( DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1)"), date('d/m') );
        })
        ->when($name == 'ngay_mai', function($query) use ($tomorrow){
            $query->where( DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1)"), date('d/m', strtotime($tomorrow)) );
        })
        ->when($date_filter != '', function($query) use ($date_filter){
            $query->where( DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1)"), $date_filter );
        })
        ->select('id', 'status', 'ngay_gio as date', 'giai as tournament', 'logo_nha as logo_home', 'ten_nha as name_home', 'tiso as score', 'logo_khach as logo_away', 'ten_khach as name_away', 'blv', 'live','Linkauto as linkauto',
            DB::raw('(CASE 
                        WHEN live = 0 THEN "" 
                        ELSE link_live 
                        END) AS link_live'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "" 
                        ELSE link_live2 
                        END) AS link_live2'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "" 
                        ELSE link_live3 
                        END) AS link_live3'),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1) as day"),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', 1) as hours"),
            DB::raw('CONCAT("https://vaohang.co/video/TVCF8.mp4") as tvc'),
            DB::raw('REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT( LOWER(REPLACE(ten_nha, " ", "-")), "-vs-", LOWER(REPLACE(ten_khach, " ", "-")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", 1), ":", "")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", -1), "/", "-")), "-", id ), char(34), ""), char(47), ""), char(92), ""), char(40), ""), char(41), "") as url')
        )->orderByRaw('ngay_cap_nhat ASC, live DESC, ngay_tao DESC')->get();

        if ( $request->sort == 'day' ) {
            $output = $output->groupBy('day');
        }

        return response()->json([
            'success' => true,
            'data'    => $output,
        ]);
    }
    
    public function get_data_link(Request $request)
    {
        $output = DB::table('vh_link')->where('hien_thi', 1)
        ->select('id', 'status', 'ngay_gio as date', 'giai as tournament', 'logo_nha as logo_home', 'ten_nha as name_home', 'tiso as score', 'logo_khach as logo_away', 'ten_khach as name_away', 'blv', 'live','Linkauto as linkauto',
            DB::raw('(CASE 
                        WHEN live = 0 THEN "" 
                        ELSE link_live 
                        END) AS link_live'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "" 
                        ELSE link_live2 
                        END) AS link_live2'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "" 
                        ELSE link_live3 
                        END) AS link_live3'),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1) as day"),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', 1) as hours"),
            DB::raw('CONCAT("https://vaohang.co/video/TVCF8.mp4") as tvc'),
            DB::raw('REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT( LOWER(REPLACE(ten_nha, " ", "-")), "-vs-", LOWER(REPLACE(ten_khach, " ", "-")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", 1), ":", "")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", -1), "/", "-")), "-", id ), char(34), ""), char(47), ""), char(92), ""), char(40), ""), char(41), "") as url')
        )->orderByRaw('live DESC, linkauto DESC, day ASC, hours ASC, ngay_tao DESC')->get();

        return response()->json([
            'success' => true,
            'data'    => $output,
        ]);
    }

    public function get_data_detail_link(Request $request, $url)
    {
        $arr_url = explode('-', $url);
        $id = end($arr_url);

        $data = array();

        $output = DB::table('vh_link')->where('hien_thi', 1)->where('id', $id)
        ->select('id', 'status', 'match_id', 'ngay_gio as date', 'giai as tournament', 'logo_nha as logo_home', 'ten_nha as name_home', 'tiso as score', 'logo_khach as logo_away', 'ten_khach as name_away', 'blv', 'live','Linkauto as linkauto',
            DB::raw('(CASE 
                        WHEN live = 0 THEN "https://vaohang.co/video/TVCF8.mp4" 
                        ELSE link_live 
                        END) AS link_live'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "https://vaohang.co/video/TVCF8.mp4" 
                        ELSE link_live2 
                        END) AS link_live2'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "https://vaohang.co/video/TVCF8.mp4" 
                        ELSE link_live3 
                        END) AS link_live3'),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1) as day"),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', 1) as hours"),
            DB::raw('CONCAT("https://vaohang.co/video/TVCF8.mp4") as tvc'),
            DB::raw('REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT( LOWER(REPLACE(ten_nha, " ", "-")), "-vs-", LOWER(REPLACE(ten_khach, " ", "-")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", 1), ":", "")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", -1), "/", "-")), "-", id ), char(34), ""), char(47), ""), char(92), ""), char(40), ""), char(41), "") as url')
        )->first();
        if ( $output && $output->match_id != '' ) {
            $data = DB::table('api_odds')->where('id_fixture', $output->match_id)->selectRaw('"F8BET" as name, home, draw, away')->get();
        }
        return response()->json([
            'success' => true,
            'data'    => $output,
            'data_odds'    => $data,
        ]);
    }
    
    public function get_list_data_link(Request $request, $url)
    {
        $limit = $request->limit == '' ? 25 : $request->limit;
        $arr_url = explode('-', $url);
        $id = end($arr_url);

        $output = DB::table('vh_link')->where('hien_thi', 1)->where('id', '!=', $id)
        ->select('id', 'status', 'ngay_gio as date', 'giai as tournament', 'logo_nha as logo_home', 'ten_nha as name_home', 'tiso as score', 'logo_khach as logo_away', 'ten_khach as name_away', 'blv', 'live','Linkauto as linkauto',
            DB::raw('(CASE 
                        WHEN live = 0 THEN "https://vaohang.co/video/TVCF8.mp4" 
                        ELSE link_live 
                        END) AS link_live'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "https://vaohang.co/video/TVCF8.mp4" 
                        ELSE link_live2 
                        END) AS link_live2'),
            DB::raw('(CASE 
                        WHEN live = 0 THEN "https://vaohang.co/video/TVCF8.mp4" 
                        ELSE link_live3 
                        END) AS link_live3'),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1) as day"),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', 1) as hours"),
            DB::raw('CONCAT("https://vaohang.co/video/TVCF8.mp4") as tvc'),
            DB::raw('REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT( LOWER(REPLACE(ten_nha, " ", "-")), "-vs-", LOWER(REPLACE(ten_khach, " ", "-")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", 1), ":", "")), "-", LOWER(REPLACE(SUBSTRING_INDEX(ngay_gio, " ", -1), "/", "-")), "-", id ), char(34), ""), char(47), ""), char(92), ""), char(40), ""), char(41), "") as url')
        )->orderByRaw('live DESC, linkauto DESC, day ASC, hours ASC, ngay_tao DESC')->paginate($limit);

        return response()->json([
            'success' => true,
            'data'    => $output,
        ]);
    }
    
    public function get_data_highlight(Request $request)
    {
        $limit = $request->limit == '' ? 25 : $request->limit;

        $output = DB::table('vh_highlight')->where('hien_thi', 1)
        ->select('id', 'ten as name', 'ngay as date', 'link_video as src_video',
            DB::raw("CONCAT('".domain()."', hinh_anh) as src_image"),
            DB::raw('CONCAT( LOWER( REPLACE(REPLACE(ten, " ", "-"), "/", "-") ), "-", id ) as url'))
        ->orderByRaw('date DESC')->paginate($limit);

        return response()->json([
            'success' => true,
            'data'    => $output,
        ]);
    }

    public function get_detail_highlight(Request $request, $url)
    {
        $arr_url = explode('-', $url);
        $id = end($arr_url);

        $output = DB::table('vh_highlight')->where('hien_thi', 1)->where('id', $id)
        ->select('id', 'ten as name', 'ngay as date', 'link_video as src_video',
            DB::raw("CONCAT('".domain()."', hinh_anh) as src_image"),
            DB::raw('CONCAT( LOWER( REPLACE(REPLACE(ten, " ", "-"), "/", "-") ), "-", id ) as url'))->first();

        $data = DB::table('vh_highlight')->where('hien_thi', 1)->where('id', '!=', $id)
        ->select('id', 'ten as name', 'ngay as date', 'hinh_anh as src_image', 'link_video as src_video',
            DB::raw('CONCAT( LOWER( REPLACE(REPLACE(ten, " ", "-"), "/", "-") ), "-", id ) as url'))
        ->orderByRaw('date DESC')->take(5)->get();

        return response()->json([
            'success' => true,
            'data'    => $output,
            'list'    => $data,
        ]);
    }
    
    public function get_data_blv(Request $request)
    {
        $limit = $request->limit == '' ? 25 : $request->limit;

        $output = DB::table('vh_blv as t1')->where('t1.hien_thi', 1)
        ->select('t1.id', 't1.ten as name',
            DB::raw("CONCAT('".domain()."', t1.hinh_anh) as src_image"),
            DB::raw("COALESCE( (select REPLACE(REPLACE(REPLACE(CONCAT( LOWER(REPLACE(t2.ten_nha, ' ' , '-')), '-vs-', LOWER(REPLACE(t2.ten_khach, ' ', '-')), '-', LOWER(REPLACE(SUBSTRING_INDEX(t2.ngay_gio, ' ', 1), ':', '')), '-', LOWER(REPLACE(SUBSTRING_INDEX(t2.ngay_gio, ' ', -1), '/', '-')), '-', id ), char(34), ''), char(47), ''), char(92), '') FROM vh_link t2 where t2.hien_thi = 1 and t2.live = 1 and t2.blv_id = t1.id limit 1) , '') link_live"))
        ->orderByRaw('name ASC')->paginate($limit);

        return response()->json([
            'success' => true,
            'data'    => $output,
        ]);
    }
}
