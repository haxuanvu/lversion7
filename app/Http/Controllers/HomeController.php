<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_view_link(Request $request)
    {
        $data_blv = DB::table('vh_blv')->where('hien_thi', 1)->get();
        $data_match = DB::table('api_all_match')->get();
        return view('pages.link', compact('data_blv', 'data_match'));
    }

    public function get_data_link(Request $request)
    {          
        DB::statement(DB::raw('set @rownum=0'));
        $output = DB::table('vh_link')->where('hien_thi', 1)
        ->where(function($query){
            $query->orWhere('status', '!=', 1)->orWhereNull('status')->orWhere('live', 1)->orWhere('Linkauto', '!=', '');
        })
        ->select('*',
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', -1) as day"),
            DB::raw("SUBSTRING_INDEX(ngay_gio, ' ', 1) as hours")
        )->orderByRaw('day ASC, hours ASC, ngay_tao DESC')->get();

        return Datatables::of($output)
        ->addColumn('STT', function($output){
            return $output->rownum;
        })
        ->addColumn('date', function($output){
            return $output->ngay_gio;
        })
        ->addColumn('home', function($output){
            return '<img src="'.$output->logo_nha.'" class="img-responsive logo_clb" alt=""> '.$output->ten_nha;
        })
        ->addColumn('score', function($output){
            if ( strpos( $output->tiso, 'http') === false ) {
                return $output->tiso;
            }
            else return '<img src="'.$output->tiso.'" alt="">';
        })
        ->addColumn('away', function($output){
            return $output->ten_khach.' <img src="'.$output->logo_khach.'" class="img-responsive logo_clb" alt="">';
        })
        ->editColumn('live', function($output){
            if ( $output->live == 0 ) {
                return '
                        <label class="switch">
                          <input class="btn_live" data-id="'.$output->id.'" type="checkbox">
                          <span class="slider round"></span>
                        </label>';
            }
            else return '
                    <label class="switch">
                      <input class="btn_live" data-id="'.$output->id.'" type="checkbox" checked>
                      <span class="slider round"></span>
                    </label>';
        })
        ->editColumn('hot', function($output){
            if ( $output->hot == 0 ) {
                return '
                        <label class="switch">
                          <input class="btn_hot" data-id="'.$output->id.'" type="checkbox">
                          <span class="slider round"></span>
                        </label>';
            }
            else return '
                    <label class="switch">
                      <input class="btn_hot" data-id="'.$output->id.'" type="checkbox" checked>
                      <span class="slider round"></span>
                    </label>';
        })
        ->addColumn('detail', function($output){
            $str_copy = '<a href="javascript:void(0)" class="btn_copy btn btn-sm btn-warning" data-com="'.$output->link.'"><i class="fa fa-clone" aria-hidden="true"></i> Copy link</a> <a href="javascript:void(0)" class="btn btn-sm btn-primary btn_edit" data-id="'.$output->id.'" data-link="'.$output->link_live.'"><i class="fa far fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="btn btn-sm btn-danger btn_delete" data-id="'.$output->id.'" ><i class="fa far fa-trash-alt"></i> Delete</a>';
            return $str_copy;
        })
        ->rawColumns(['STT', 'date', 'home', 'score', 'away', 'live', 'hot', 'detail'])
        ->editColumn('id', '{{$id}}')
        ->setRowId('id')
        ->make(true);
    }

    public function get_edit_link(Request $request)
    {
        $output = DB::table('vh_link')->where('id', $request->id)->first();
        return response()->json($output);
    }

    public function post_data_link(Request $request)
    {
      //  dd('post_data_link');
        // $items = DB::table('vh_link')->where('id', $request->id)->first();
        // if ( $items ) {
        //     $item_blv = DB::table('vh_blv')->where('id', $request->blv)->first();
        //     if ( $item_blv ) {
        //         $blv = $item_blv->ten;
        //         $blv_id = $item_blv->id;
        //     }
        //     else{
        //         $blv = '';
        //         $blv_id = '';
        //     }

        //     $arr_live = explode('/', $request->Linkauto);
        //     $link_live = isset($arr_live[3]) ? 'https://vaohang.vip/'.$arr_live[3].'/data/test.m3u8' : '';

        //     if ($items->status == 1) {
        //         DB::table('vh_link')->where('id', $request->id)->update([
        //             'link_live'=>$link_live,
        //             'link_live2'=>$request->link_live2,
        //             'link_live3'=>$request->link_live3,
        //             'Linkauto'=>$request->Linkauto,
        //             'blv'=> $blv,
        //             'blv_id'=> $blv_id,
        //             'match_id'=> $request->match_id,
        //         ]);
        //     }
        //     else{
        //         $ngay_gio = date('H:i d/m', strtotime($request->ngay_gio));
        //         DB::table('vh_link')->where('id', $request->id)->update([
        //             'ngay_gio' => $ngay_gio,
        //             'giai' => $request->giai,
        //             'ten_nha' => $request->ten_nha,
        //             'logo_nha' => $request->logo_nha,
        //             'ten_khach' => $request->ten_khach,
        //             'logo_khach' => $request->logo_khach,
        //             'tiso' => $request->tiso,
        //             'link' => $request->link,
        //             'link_live' => $link_live,
        //             'link_live2' => $request->link_live2,
        //             'link_live3' => $request->link_live3,
        //             'Linkauto' => $request->Linkauto,
        //             'blv'=> $blv,
        //             'blv_id'=> $blv_id,
        //             'match_id'=> $request->match_id,
        //             'ngay_cap_nhat' => date('Y-m-d H:i:s', strtotime($request->ngay_gio))
        //         ]);
        //     }
        // }

       // return response()->json(['success'=>'Cập nhật thành công']);
    }

    public function post_add_link(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ngay_gio'  => 'required',
            'giai'  => 'required',
            'ten_nha'  => 'required',
            'ten_khach'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>'Vui lòng cập nhật thông tin bắt buộc']);
        }

        $item_blv = DB::table('vh_blv')->where('id', $request->blv)->first();
        if ( $item_blv ) {
            $blv = $item_blv->ten;
            $blv_id = $item_blv->id;
        }
        else{
            $blv = '';
            $blv_id = '';
        }

        $arr_live = explode('/', $request->Linkauto);
        $link_live = isset($arr_live[3]) ? 'https://vaohang.vip/'.$arr_live[3].'/data/test.m3u8' : '';

        $ngay_gio = date('H:i d/m', strtotime($request->ngay_gio));
        DB::table('vh_link')->insert([
            'ngay_gio' => $ngay_gio,
            'giai' => $request->giai,
            'ten_nha' => $request->ten_nha,
            'logo_nha' => $request->logo_nha,
            'ten_khach' => $request->ten_khach,
            'logo_khach' => $request->logo_khach,
            'tiso' => $request->tiso,
            'link' => $request->link,
             'hot' => 0,
            'link_live' => $link_live,
            'link_live2' => $request->link_live2,
            'link_live3' => $request->link_live3,
            'Linkauto' => $request->Linkauto,
            'blv'=> $blv,
            'blv_id'=>  $blv_id,
            'match_id'=> $request->match_id,
            'hien_thi' => 1,
            'status' => 0,
            'ngay_tao' => date('Y-m-d H:i:s'),
            'ngay_cap_nhat' => date('Y-m-d H:i:s', strtotime($request->ngay_gio))
        ]);

        return response()->json(['success'=>'Cập nhật thành công']);
    }

    public function post_live_link(Request $request)
    {
        DB::table('vh_link')->where('id', $request->id)->update(['live'=> $request->live]);
        return response()->json(['success'=>'Cập nhật thành công']);
    }

    public function post_hot_link(Request $request)
    {
        DB::table('vh_link')->where('id', $request->id)->update(['hot'=> $request->hot]);
        return response()->json(['success'=>'Cập nhật thành công']);
    }

    public function post_delete_link(Request $request)
    {
        DB::table('vh_link')->where('id', $request->id)->update(['hien_thi'=> 0]);
        return response()->json(['success'=>'Cập nhật thành công']);
    }


    public function get_data_highlight(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $output = DB::table('vh_highlight')->select('*',
            DB::raw('@rownum  := @rownum  + 1 AS rownum')
        )->orderByRaw('ngay DESC, ngay_tao DESC')->get();

        return Datatables::of($output)
        ->addColumn('STT', function($output){
            return $output->rownum;
        })
        ->addColumn('date', function($output){
            return $output->ngay;
        })
        ->addColumn('name', function($output){
            return $output->ten;
        })
        ->addColumn('display', function($output){
            if ( $output->hien_thi == 0 ) {
                return '
                        <label class="switch">
                          <input class="btn_display" data-id="'.$output->id.'" type="checkbox">
                          <span class="slider round"></span>
                        </label>';
            }
            else return '
                    <label class="switch">
                      <input class="btn_display" data-id="'.$output->id.'" type="checkbox" checked>
                      <span class="slider round"></span>
                    </label>';
        })
        ->addColumn('detail', function($output){
            $str_copy = '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn_edit" data-id="'.$output->id.'"><i class="fa far fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="btn btn-sm btn-danger btn_delete" data-id="'.$output->id.'" ><i class="fa far fa-trash-alt"></i> Delete</a>';
            return $str_copy;
        })
        ->rawColumns(['STT', 'date', 'ten', 'display', 'detail'])
        ->editColumn('id', '{{$id}}')
        ->setRowId('id')
        ->make(true);
    }

    public function post_data_highlight(Request $request)
    {

        $request->link_video = str_replace("youtu.be/", "youtube.com/embed/", $request->link_video);
        $request->link_video = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $request->link_video);

        if ( $request->button_action == 'insert' ) {
            $validator = Validator::make($request->all(), [
                'ngay'  => 'required',
                'ten'  => 'required',
                'hinh_anh'  => 'required',
                'link_video'  => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>'Vui lòng cập nhật thông tin bắt buộc']);
            }

            if($request->hasFile('video')){
                $file = $request->file('video');
                $file_type = mb_strtolower($file->getClientOriginalExtension());
                if($file_type != 'mp4' && $file_type != 'avi' && $file_type != 'mov' && $file_type !='wmv'){
                    return response()->json(['errors'=>'Bạn chỉ được chọn file *.mp4;*.avi;*.wmv;*.mov']);
                }
                $get_file = rand() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/video'), $get_file);

                $name_video = 'http://vaohang.co/upload/video/'.$get_file;
            }
            else{
                $name_video = '';
            }

            if($request->hasFile('hinh_anh')){
                $file = $request->file('hinh_anh');
                $file_type = mb_strtolower($file->getClientOriginalExtension());
                if($file_type != 'jpg' && $file_type != 'png' && $file_type != 'gif' && $file_type !='jpeg'){
                    return response()->json(['errors'=>'Bạn chỉ được chọn file *.jpg;*.png;*.jpeg;*.gif']);
                }
                $get_file = rand() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/avatar'), $get_file);

                $name_img = 'http://vaohang.co/upload/avatar/'.$get_file;
            }
            else{
                return response()->json(['errors'=>'Vui lòng chọn hình ảnh']);
            }

            DB::table('vh_highlight')->insert([
                'ngay' => $request->ngay,
                'ten' => $request->ten,
                'link_video' => $request->link_video,
                'video' => $name_video,
                'hinh_anh' => $name_img,
                'hien_thi' => 1,
                'ngay_tao' => date('Y-m-d H:i:s'),
                'ngay_cap_nhat' => date('Y-m-d H:i:s')
            ]);

            return response()->json(['success'=>'Cập nhật thành công']);
        }

        if ( $request->button_action == 'update' ) {
            $validator = Validator::make($request->all(), [
                'ngay'  => 'required',
                'ten'  => 'required',
                'link_video'  => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>'Vui lòng cập nhật thông tin bắt buộc']);
            }

            $highlight = DB::table('vh_highlight')->where('id', $request->id)->first();
            if ( $highlight ) {
                if($request->hasFile('video')){
                    $file = $request->file('video');
                    $file_type = mb_strtolower($file->getClientOriginalExtension());
                    if($file_type != 'mp4' && $file_type != 'avi' && $file_type != 'mov' && $file_type !='wmv'){
                        return response()->json(['errors'=>'Bạn chỉ được chọn file *.mp4;*.avi;*.wmv;*.mov']);
                    }

                    if($highlight->video != "" && file_exists($highlight->video)){
                        unlink($highlight->video);
                    }


                    $get_file = rand() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/video'), $get_file);

                    $name_video = '/upload/video/'.$get_file;
                }
                else{
                    $name_video = $highlight->video;
                }

                if($request->hasFile('hinh_anh')){
                    $file = $request->file('hinh_anh');
                    $file_type = mb_strtolower($file->getClientOriginalExtension());
                    if($file_type != 'jpg' && $file_type != 'png' && $file_type != 'gif' && $file_type !='jpeg'){
                        return response()->json(['errors'=>'Bạn chỉ được chọn file *.jpg;*.png;*.jpeg;*.gif']);
                    }

                    if($highlight->hinh_anh != "" && file_exists($highlight->hinh_anh)){
                        unlink($highlight->hinh_anh);
                    }


                    $get_file = rand() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/avatar'), $get_file);

                    $name_img = '/upload/avatar/'.$get_file;
                }
                else{
                    $name_img = $highlight->hinh_anh;
                }

                DB::table('vh_highlight')->where('id', $request->id)->update([
                    'ngay' => $request->ngay,
                    'ten' => $request->ten,
                    'link_video' => $request->link_video,
                    'video' => $name_video,
                    'hinh_anh' => $name_img,
                    'ngay_cap_nhat' => date('Y-m-d H:i:s')
                ]);
            }
            else{
                return response()->json(['errors'=>'Cập nhật không thành công']);
            }

            return response()->json(['success'=>'Cập nhật thành công']);
        }

        return response()->json(['errors'=>'Cập nhật không thành công']);
    }

    public function get_edit_highlight(Request $request)
    {
        $output = DB::table('vh_highlight')->where('id', $request->id)->first();
        return response()->json($output);
    }

    public function post_display_highlight(Request $request)
    {
        DB::table('vh_highlight')->where('id', $request->id)->update(['hien_thi'=> $request->display]);
        return response()->json(['success'=>'Cập nhật thành công']);
    }

    public function post_delete_highlight(Request $request)
    {
        $highlight = DB::table('vh_highlight')->where('id', $request->id)->first();
        if ( $highlight ) {
            if($highlight->video != "" && file_exists($highlight->video)){
                unlink($highlight->video);
            }
            DB::table('vh_highlight')->where('id', $request->id)->delete();
        }
        return response()->json(['success'=>'Cập nhật thành công']);
    }


    public function get_data_blv(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $output = DB::table('vh_blv')->select('*',
            DB::raw('@rownum  := @rownum  + 1 AS rownum')
        )->orderByRaw('ngay_tao DESC')->get();

        return Datatables::of($output)
        ->addColumn('STT', function($output){
            return $output->rownum;
        })
        ->addColumn('avatar', function($output){
            return '<img src="'.$output->hinh_anh.'" class="img-responsive" alt="" style="height: 80px; margin: auto;">';
        })
        ->addColumn('name', function($output){
            return $output->ten;
        })
        ->addColumn('luong', function($output){
            return $output->luong;
        })
        ->addColumn('display', function($output){
            if ( $output->hien_thi == 0 ) {
                return '
                        <label class="switch">
                          <input class="btn_display" data-id="'.$output->id.'" type="checkbox">
                          <span class="slider round"></span>
                        </label>';
            }
            else return '
                    <label class="switch">
                      <input class="btn_display" data-id="'.$output->id.'" type="checkbox" checked>
                      <span class="slider round"></span>
                    </label>';
        })
        ->addColumn('detail', function($output){
            $str_copy = '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn_edit" data-id="'.$output->id.'"><i class="fa far fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="btn btn-sm btn-danger btn_delete" data-id="'.$output->id.'" ><i class="fa far fa-trash-alt"></i> Delete</a>';
            return $str_copy;
        })
        ->rawColumns(['STT', 'avatar', 'name','luong', 'display', 'detail'])
        ->editColumn('id', '{{$id}}')
        ->setRowId('id')
        ->make(true);
    }

    public function post_data_blv(Request $request)
    {

        if ( $request->button_action == 'insert' ) {
            $validator = Validator::make($request->all(), [
                'ten'  => 'required',
                'hinh_anh'  => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>'Vui lòng cập nhật thông tin bắt buộc']);
            }

            if($request->hasFile('hinh_anh')){
                $file = $request->file('hinh_anh');
                $file_type = mb_strtolower($file->getClientOriginalExtension());
                if($file_type != 'jpg' && $file_type != 'png' && $file_type != 'gif' && $file_type !='jpeg'){
                    return response()->json(['errors'=>'Bạn chỉ được chọn file *.jpg;*.png;*.jpeg;*.gif']);
                }
                $get_file = rand() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/avatar'), $get_file);

                $name_img = 'http://vaohang.co/upload/avatar/'.$get_file;
            }
            else{
                return response()->json(['errors'=>'Vui lòng chọn hình ảnh']);
            }

            DB::table('vh_blv')->insert([
                'ten' => $request->ten,
                'hinh_anh' => $name_img,
                'hien_thi' => 1,
                'luong'=>$request->luong,
                'ngay_tao' => date('Y-m-d H:i:s'),
                'ngay_cap_nhat' => date('Y-m-d H:i:s')
            ]);

            return response()->json(['success'=>'Cập nhật thành công']);
        }

        if ( $request->button_action == 'update' ) {
            $validator = Validator::make($request->all(), [
                'ten'  => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>'Vui lòng cập nhật thông tin bắt buộc']);
            }

            $blv = DB::table('vh_blv')->where('id', $request->id)->first();
            if ( $blv ) {

                if($request->hasFile('hinh_anh')){
                    $file = $request->file('hinh_anh');
                    $file_type = mb_strtolower($file->getClientOriginalExtension());
                    if($file_type != 'jpg' && $file_type != 'png' && $file_type != 'gif' && $file_type !='jpeg'){
                        return response()->json(['errors'=>'Bạn chỉ được chọn file *.jpg;*.png;*.jpeg;*.gif']);
                    }

                    if($blv->hinh_anh != "" && file_exists($blv->hinh_anh)){
                        unlink($blv->hinh_anh);
                    }


                    $get_file = rand() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/avatar'), $get_file);

                    $name_img = '/upload/avatar/'.$get_file;
                }
                else{
                    $name_img = $blv->hinh_anh;
                }

                DB::table('vh_blv')->where('id', $request->id)->update([
                    'ten' => $request->ten,
                    'hinh_anh' => $name_img,
                    'luong'=>$request->luong,
                    'ngay_cap_nhat' => date('Y-m-d H:i:s')
                ]);
            }
            else{
                return response()->json(['errors'=>'Cập nhật không thành công']);
            }

            return response()->json(['success'=>'Cập nhật thành công']);
        }

        return response()->json(['errors'=>'Cập nhật không thành công']);
    }

    public function get_edit_blv(Request $request)
    {
        $output = DB::table('vh_blv')->where('id', $request->id)->first();
        return response()->json($output);
    }

    public function post_display_blv(Request $request)
    {
        DB::table('vh_blv')->where('id', $request->id)->update(['hien_thi'=> $request->display]);
        return response()->json(['success'=>'Cập nhật thành công']);
    }

    public function post_delete_blv(Request $request)
    {
        DB::table('vh_blv')->where('id', $request->id)->delete();
        return response()->json(['success'=>'Cập nhật thành công']);
    }
}
