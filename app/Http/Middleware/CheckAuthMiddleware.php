<?php

namespace App\Http\Middleware;

use Auth;
use DB;
use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class CheckAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!Auth::check()) {
            Auth::logout();
            return Redirect::Route('login')->withErrors([
                'message' => 'Tài khoản không tồn tại!',
            ]);
        }

        return $next($request);
    }
}
