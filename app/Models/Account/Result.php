<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Result extends Model
{
    protected $table = 'vh_link';
    protected $fillable = [
        'ngay_gio', 'giai', 'logo_nha', 'ten_nha', 'tiso', 'ten_khach', 'logo_khach', 'link', 'trang_thai', 'ngay_tao', 'ngay_cap_nhat'
    ];
    public $timestamps = false;


}
