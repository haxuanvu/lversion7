

@extends('master')
@section('content')
<html lang="en">
<head>
    <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <style>


        .switch {
          position: relative;
          display: inline-block;
          width: 50px;
          height: 24px;
        }

        .switch input { 
          opacity: 0;
          width: 0;
          height: 0;
        }

        .switch .slider {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
        }

        .switch .slider:before {
          position: absolute;
          content: "";
          height: 18px;
          width: 18px;
          left: 4px;
          bottom: 3px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
        }

        input:checked + .slider {
          background-color: #2196F3;
        }

        input:focus + .slider {
          box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
          -webkit-transform: translateX(18px);
          -ms-transform: translateX(18px);
          transform: translateX(18px);
        }

        /* Rounded sliders */
        .switch .slider.round {
          border-radius: 34px;
        }

        .switch .slider.round:before {
          border-radius: 50%;
        }

        main{
            position: relative;
        }
        img.loading-img{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 99999;
            width: 250px;
            height: auto;
        }
        img.loading-img.not_show{
            display: none;
        }
    </style>
</head>
<body>
    <section class="content">
        <img src="{{ asset('video/loading.gif') }}" alt="" class="loading-img not_show">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="fa far fa-arrow-circle-left"></i>Home</a></li>
                </ol>
            </div>
            <div class="col-md-12">
                <div class="container-fluid">

                    <div class="pull-right" style="margin-bottom: 20px;">
                        <div class="btn_add"><span class="btn btn-sm btn-info"><i class="fas fa-plus"></i> Thêm mới</span></div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="box table table-bordered table-striped table-hover display" cellspacing="0" id="table">
                            <thead>
                                <tr>
                                    <th> STT </th>
                                    <th>Ngày thi đấu</th>
                                    <th>Trận đấu</th>
                                    <th>Hiển thị</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="group_customer" class="modal fade" role="dialog"  data-backdrop="false" style="font-size: 13px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="group_customer_form"  enctype="multipart/form-data">
                    <div class="modal-header  bg-primary">
                        <h6 class="modal-title title_add">Thêm mới</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body modal-body-new">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Ngày thi đấu <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" name="ngay" id="ngay" value="{{ date('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Trận đấu <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ten" id="ten"  placeholder="Trận đấu">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Hình ảnh <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <div id="group_image"></div>
                                <input type="file" class="form-control" name="hinh_anh" id="hinh_anh" placeholder="Hình ảnh">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Video Highlight <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <div id="group_video"></div>
                                <input type="text" class="form-control" name="link_video" id="link_video" placeholder="Video Highlight">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="" />
                        <button type="submit" name="submit" id="action_add" class="btn btn-primary">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true,
            });

            $(function() {
                url = '{!! route('admin.data.highlight') !!}';
                // url = url.replace("http://", "https://");
                var t =  $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url :   url,
                        type: "GET",
                        dataType: "json",
                    },
                    columns: [
                        { data: 'STT', className: 'text-center'},
                        { data: 'date', className: 'text-center'},
                        { data: 'name', className: 'text-left'},
                        { data: 'display', className: 'text-center'},
                        { data: 'detail', className: 'text-center'},
                    ],

                    "iDisplayLength": 25,
                    "language": {
                        "url": "Vietnamese.json"
                    },
                });
            });
        });


            // Edit
            $(document).on('click', '.btn_edit', function(){
                $('#group_customer_form')[0].reset();
                var id = $(this).data("id");
                url = "{{ route('admin.edit.highlight') }}";
                // url = url.replace("http://", "https://");
                $.ajax({
                    url: url,
                    method:"GET",
                    data:{id:id, _token: '{{csrf_token()}}'},
                    dataType:'JSON',
                    success:function(data)
                    {
                        $('.title_add').html('Chỉnh sửa');
                        $('#id').val(data.id);
                        $('#ngay').val(data.ngay);
                        $('#ten').val(data.ten);
                        $('#link_video').val(data.link_video);

                        $('#group_video').html('<iframe width="250" height="140" src="'+data.link_video+'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                        // $('#group_video').html('<video src="'+data.link_video+'" style="width: 250px; height: auto;" autoplay=""></video>');
                        $('#group_image').html('<img src="'+data.hinh_anh+'" style="width: 250px; height: auto;"></img>');

                        $('#button_action').val('update');
                        $('#group_customer').modal('show');
                    }
                })
            });
            

            $(document).on('click', '.btn_add', function(){
                $('#id').val('');
                $('.title_add').html('Thêm mới');
                $('#group_customer_form')[0].reset();
                $('#group_video').html('');
                $('#group_image').html('');
                $('#button_action').val('insert');
                $('#group_customer').modal('show');
            });


            $('#group_customer_form').on('submit', function(event){
                $('.loading-img').removeClass('not_show');
                event.preventDefault();

                url = "{{ route('admin.update.highlight') }}";
                // url = url.replace("http://", "https://");
                $.ajax({
                    url: url,
                    method:"POST",
                    data:new FormData(this),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        if (typeof data.errors !== 'undefined') {
                            $('.loading-img').addClass('not_show');
                            swal(data.errors, {
                                icon: "error",
                                position: 'top-end',
                                timer: 3000
                            });
                        }
                        else{
                            $('.loading-img').addClass('not_show');
                            $('#group_video').html('');
                            $('#table').DataTable().ajax.reload(null, false);
                            $('#group_customer').modal('hide');
                        }
                    },
                    error: function(data) {
                        $('.loading-img').addClass('not_show');
                        swal('Video có dung lượng quá lớn. Vui lòng chọn video có dung lượng thấp hơn.', {
                            icon: "error",
                            position: 'top-end',
                            timer: 3000
                        });
                    }
                })
            });


            $(document).on('click', '.btn_delete', function(){
                var id = $(this).data("id");
                var table = $('#table').DataTable();
                str_name = '';
                table.rows().eq(0).each( function ( index ) {
                    var row = table.row( index );
                    var data = row.data();
                    if(id == data['id']){
                        str_name = data['name'];
                    }
                } );
                swal({
                    title: "Bạn muốn xóa highlight trận đấu:",
                    text: str_name,
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        url = "{{ route('admin.delete.highlight') }}";
                        // url = url.replace("http://", "https://");
                        $.ajax({
                            url: url,
                            method:'POST',
                            data:{id:id, _token: '{{csrf_token()}}'},
                            dataType:'JSON',
                            success:function(data)
                            {
                                $('#table').DataTable().ajax.reload(null, false);
                            }
                        })
                        swal("Đã xóa!", {
                            icon: "success",
                        });
                    }
                });
            });
            $(document).on('click', '.btn_display', function(){
                var id = $(this).data("id");
                if( $(this).is(':checked') ){
                    display = 1;
                }else{
                    display = 0;
                }
                url = "{{ route('admin.display.highlight') }}";
                // url = url.replace("http://", "https://");
                $.ajax({
                    url: url,
                    method:"POST",
                    data:{id:id, display:display, _token: '{{csrf_token()}}'},
                    dataType:'JSON'
                })
            });

            
            $(document).on('click', '.close', function(){
                $('#group_video').html('');
            });
    </script>
</body>
</html>
@endsection
