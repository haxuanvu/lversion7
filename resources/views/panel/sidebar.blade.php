<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.home') }}"><i class="fa fas fa-home"></i> Home </a>
      </li>
      <li class="navigation-header"><span>Quản lý</span> </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/link*') ? 'active' : '' }}"  href="{{ route('admin.link') }}"><i class="fas fa-calendar-alt"></i>Trận đấu</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/highlight*') ? 'active' : '' }}"  href="{{ route('admin.highlight') }}"><i class="fas fa-video"></i>Highlight</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/blv*') ? 'active' : '' }}"  href="{{ route('admin.blv') }}"><i class="fas fa-user-secret"></i>Bình luận viên</a>
      </li>
      <div>
        &nbsp;
      </div>
    </li>
  </ul>
</nav>
</div>
