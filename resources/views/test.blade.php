

@extends('master')
@section('content')
<html lang="en">
<head>
    <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <style>
        .logo_clb{
            display: inline-block !important;
            width: 50px !important;
        }


        .switch {
          position: relative;
          display: inline-block;
          width: 50px;
          height: 24px;
        }

        .switch input { 
          opacity: 0;
          width: 0;
          height: 0;
        }

        .switch .slider {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
        }

        .switch .slider:before {
          position: absolute;
          content: "";
          height: 18px;
          width: 18px;
          left: 4px;
          bottom: 3px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
        }

        input:checked + .slider {
          background-color: #2196F3;
        }

        input:focus + .slider {
          box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
          -webkit-transform: translateX(18px);
          -ms-transform: translateX(18px);
          transform: translateX(18px);
        }

        /* Rounded sliders */
        .switch .slider.round {
          border-radius: 34px;
        }

        .switch .slider.round:before {
          border-radius: 50%;
        }
    </style>
</head>
<body>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="fa far fa-arrow-circle-left"></i>Home</a></li>
                </ol>
            </div>
            <div class="col-md-12">
                <div class="container-fluid">

                    <div class="pull-right" style="margin-bottom: 20px;">
                        <div class="btn_add"><span class="btn btn-sm btn-info"><i class="fas fa-plus"></i> Thêm mới</span></div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="box table table-bordered table-striped table-hover display" cellspacing="0" id="table">
                            <thead>
                                <tr>
                                    <th> STT </th>
                                    <th>Ngày thi đấu</th>
                                    <th>Sân nhà</th>
                                    <th>Tỉ số</th>
                                    <th>Sân khách</th>
                                    <th>Bình luận viên</th>
                                    <th>Trực tiếp</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="group_customer" class="modal fade" role="dialog"  data-backdrop="false" style="font-size: 13px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="group_customer_form" >
                    <div class="modal-header  bg-primary">
                        <h6 class="modal-title">Chỉnh sửa</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body modal-body-new">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link livestream</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link_live" id="edit_link_live" placeholder="Link livestream">
                            </div>
                        </div>
                    </div>
                    <div class="modal-body modal-body-new">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link livestream 2</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link_live2" id="edit_link_live2" placeholder="Link livestream 2">
                            </div>
                        </div>
                    </div>
                    <div class="modal-body modal-body-new">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link livestream 3</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link_live3" id="edit_link_live3" placeholder="Link livestream 3">
                            </div>
                        </div>
                    </div>
                    <div class="modal-body modal-body-new">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Bình luận viên</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="blv" id="edit_blv" placeholder="Bình luận viên">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="edit_id" value="" />
                        <button type="submit" name="submit" id="action" class="btn btn-primary">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="group_customer_add" class="modal fade" role="dialog"  data-backdrop="false" style="font-size: 13px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="group_customer_form_add"  enctype="multipart/form-data">
                    <div class="modal-header  bg-primary">
                        <h6 class="modal-title title_add">Thêm mới</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body modal-body-new">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Giờ đấu <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ngay_gio" id="ngay_gio"  placeholder="Giờ đấu. Vd: 19:30 22/11">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Giải đấu <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="giai" id="giai"  placeholder="Giải đấu">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Tên đội nhà <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ten_nha" id="ten_nha"  placeholder="Tên đội nhà">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Logo đội nhà</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="logo_nha" id="logo_nha" placeholder="Logo đội nhà">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Tên đội khách <span style="color: red">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ten_khach" id="ten_khach"  placeholder="Tên đội khách">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Logo đội khách</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="logo_khach" id="logo_khach" placeholder="Logo đội khách">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Tỉ số</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tiso" id="tiso" placeholder="Tỉ số. Vd: 0-0">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link nguồn</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link" id="link" placeholder="Link nguồn">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link livestream</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link_live" id="link_live" placeholder="Link livestream">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link livestream 2</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link_live2" id="link_live2" placeholder="Link livestream 2">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Link livestream 3</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="link_live3" id="link_live3" placeholder="Link livestream 3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Bình luận viên</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="blv" id="blv" placeholder="Bình luận viên">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="" />
                        <button type="submit" name="submit" id="action_add" class="btn btn-primary">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true,
            });

            $(function() {
                var t =  $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url :   '{!! route('admin.data.link') !!}',
                        type: "GET",
                        dataType: "json",
                    },
                    columns: [
                        { data: 'STT', className: 'text-center'},
                        { data: 'date', className: 'text-center'},
                        { data: 'home', className: 'text-left'},
                        { data: 'score', className: 'text-center'},
                        { data: 'away', className: 'text-right'},
                        { data: 'blv', className: 'text-center'},
                        { data: 'live', className: 'text-center'},
                        { data: 'detail', className: 'text-center'},
                    ],

                    "iDisplayLength": 25,
                    "language": {
                        "url": "Vietnamese.json"
                    },
                });
            });
        });


        $(document).on('click', '.btn_copy', function(){
            $('.btn_copy').removeClass('btn-success').addClass('btn-warning');
            str = $(this).data('com');
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(str).select();
            var copyStatus = document.execCommand("copy");
            $temp.remove();
            $(this).removeClass('btn-warning').addClass('btn-success');
        });


            $('#group_customer_form').on('submit', function(event){
                event.preventDefault();
                $.ajax({
                    url:"{{ route('admin.update.link') }}",
                    method:"POST",
                    data:new FormData(this),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        $('#table').DataTable().ajax.reload(null, false);
                        $('#group_customer').modal('hide');
                        $('#group_customer_add').modal('hide');
                    }
                })
            });
            // Edit
            $(document).on('click', '.btn_edit', function(){
                var id = $(this).data("id");
                $.ajax({
                    url:"{{ route('admin.edit.link') }}",
                    method:"GET",
                    data:{id:id, _token: '{{csrf_token()}}'},
                    dataType:'JSON',
                    success:function(data)
                    {
                        if (data.trang_thai != '' && data.trang_thai != null) {
                            $('#edit_id').val(data.id);
                            $('#edit_link_live').val(data.link_live);
                            $('#edit_link_live2').val(data.link_live2);
                            $('#edit_link_live3').val(data.link_live3);
                            $('#edit_blv').val(data.blv);

                            $('#group_customer').modal('show');
                        }
                        else{
                            $('.title_add').html('Chỉnh sửa');
                            $('#id').val(data.id);
                            $('#ngay_gio').val(data.ngay_gio);
                            $('#giai').val(data.giai);
                            $('#logo_nha').val(data.logo_nha);
                            $('#ten_nha').val(data.ten_nha);
                            $('#logo_khach').val(data.logo_khach);
                            $('#ten_khach').val(data.ten_khach);
                            $('#tiso').val(data.tiso);
                            $('#link').val(data.link);
                            $('#link_live').val(data.link_live);
                            $('#link_live2').val(data.link_live2);
                            $('#link_live3').val(data.link_live3);
                            $('#blv').val(data.blv);

                            $('#group_customer_add').modal('show');
                        }
                    }
                })
            });
            

            $(document).on('click', '.btn_add', function(){
                $('#id').val('');
                $('.title_add').html('Thêm mới');
                $('#group_customer_form_add')[0].reset();
                $('#group_customer_add').modal('show');
            });
            $('#group_customer_form_add').on('submit', function(event){
                if ( $('#id').val() == '' ) {
                    srt_url = "{{ route('admin.create.link') }}";
                }
                else srt_url = "{{ route('admin.update.link') }}";
                event.preventDefault();
                $.ajax({
                    url: srt_url,
                    method:"POST",
                    data:new FormData(this),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        if (typeof data.errors !== 'undefined') {
                            swal(data.errors, {
                                icon: "error",
                                position: 'top-end',
                                timer: 3000
                            });
                        }
                        else{
                            $('#table').DataTable().ajax.reload(null, false);
                            $('#group_customer_add').modal('hide');
                        }
                    }
                })
            });

            $(document).on('click', '.btn_delete', function(){
                var id = $(this).data("id");
                var table = $('#table').DataTable();
                str_name = '';
                table.rows().eq(0).each( function ( index ) {
                    var row = table.row( index );
                    var data = row.data();
                    if(id == data['id']){
                        str_name = data['ten_nha']+' - '+data['ten_khach'];
                    }
                } );
                swal({
                    title: "Bạn thật sự muốn xóa trận đấu:",
                    text: str_name,
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            url:"{{ route('admin.delete.link') }}",
                            method:'POST',
                            data:{id:id, _token: '{{csrf_token()}}'},
                            dataType:'JSON',
                            success:function(data)
                            {
                                $('#table').DataTable().ajax.reload(null, false);
                            }
                        })
                        swal("Đã xóa!", {
                            icon: "success",
                        });
                    }
                });
            });


            $(document).on('click', '.btn_live', function(){
                var id = $(this).data("id");
                if( $(this).is(':checked') ){
                    live = 1;
                }else{
                    live = 0;
                }
                console.log(id, live);
                $.ajax({
                    url:"{{ route('admin.live.link') }}",
                    method:"POST",
                    data:{id:id, live:live, _token: '{{csrf_token()}}'},
                    dataType:'JSON'
                })
            });
    </script>
</body>
</html>
@endsection
