<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['cors']], function ()
{
    Route::group(['namespace' => 'API'], function () {
        Route::get('statistical', 'HomeController@get_statistical');
        Route::get('statistical/{name}', 'HomeController@get_statistical_link');

        Route::get('live', 'HomeController@get_data_link');
        Route::get('live/{url}', 'HomeController@get_data_detail_link');
        Route::get('list-live/{url}', 'HomeController@get_list_data_link');

        Route::get('highlight', 'HomeController@get_data_highlight');
        Route::get('highlight-detail/{url}', 'HomeController@get_detail_highlight');

        Route::get('blv', 'HomeController@get_data_blv');
    });
});
