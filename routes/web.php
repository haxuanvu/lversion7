<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
/* CoreUI templates */

Route::group(['prefix' => '', "as" => 'admin.', 'middleware' => ['auth', 'admin']], function () {
	/*Route::view('', 'pages.link')->name('home');
	Route::view('link', 'pages.link')->name('link');*/
	Route::get('', 'HomeController@get_view_link')->name('home');
	Route::get('link', 'HomeController@get_view_link')->name('link');
	
	Route::get('edit-link', 'HomeController@get_edit_link')->name('edit.link');
	Route::post('data-link', 'HomeController@post_data_link')->name('update.link');
	Route::post('add-link', 'HomeController@post_add_link')->name('create.link');
	Route::post('live-link', 'HomeController@post_live_link')->name('live.link');
	Route::post('hot-link', 'HomeController@post_hot_link')->name('hot.link');
	Route::post('delete-link', 'HomeController@post_delete_link')->name('delete.link');

	Route::view('highlight', 'pages.highlight')->name('highlight');
	
	Route::post('data-highlight', 'HomeController@post_data_highlight')->name('update.highlight');
	Route::get('edit-highlight', 'HomeController@get_edit_highlight')->name('edit.highlight');
	Route::post('display-highlight', 'HomeController@post_display_highlight')->name('display.highlight');
	Route::post('delete-highlight', 'HomeController@post_delete_highlight')->name('delete.highlight');

	Route::view('blv', 'pages.blv')->name('blv');
	
	Route::post('data-blv', 'HomeController@post_data_blv')->name('update.blv');
	Route::get('edit-blv', 'HomeController@get_edit_blv')->name('edit.blv');
	Route::post('display-blv', 'HomeController@post_display_blv')->name('display.blv');
	Route::post('delete-blv', 'HomeController@post_delete_blv')->name('delete.blv');

	Route::get('data-link', 'HomeController@get_data_link')->name('data.link');
	Route::get('data-highlight', 'HomeController@get_data_highlight')->name('data.highlight');
	Route::get('data-blv', 'HomeController@get_data_blv')->name('data.blv');

	/*Route::group(['scheme' => 'https'], function () {
		Route::get('data-link', 'HomeController@get_data_link')->name('data.link');
		Route::get('data-highlight', 'HomeController@get_data_highlight')->name('data.highlight');
		Route::get('data-blv', 'HomeController@get_data_blv')->name('data.blv');
	});*/

});

// Route::view('', 'welcome')->name('home');
// Section Pages
Route::view('/sample/error404','errors.404')->name('error404');
Route::view('/sample/error500','errors.500')->name('error500');